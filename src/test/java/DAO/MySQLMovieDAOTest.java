package DAO;

import DTO.Movie;
import Exceptions.DAOException;
import org.junit.Test;

import static org.junit.Assert.*;

public class MySQLMovieDAOTest {

//    @Test
//    public void findMovieByTitleDirector() throws DAOException {
//        System.out.println("findMovieByTitleDirector");
//        String title = "elektra";
//        String director = "Rob Bowman";
//        MySQLMovieDAO instance = new MySQLMovieDAO();
//        Movie expResult = new Movie("elektra", "Rob Bowman");
//        String result = instance.findMovieByTitleDirector(title, director);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }

    @Test
    public void addMovie() throws DAOException {
        System.out.println("Add Movie");
        String title = "iron man";
        String genre = "Marvel, Action, Adventure, Sci-Fi";
        String director = "Jon Favreau";
        String runtime = "126 min";
        String plot = "Tony Stark. Genius, billionaire, playboy, philanth...";
        String location = null;
        String rating = "PG-13";
        String format = "DVD";
        String year = "2008";
        String starring = "Robert Downey Jr., Terrence Howard, Jeff Bridges, ...";
        int copies = 3;
        String barcode = null;
        String user_rating = null;
        MySQLMovieDAO instance = new MySQLMovieDAO();
        Movie expResult = new Movie("iron man", "Marvel, Action, Adventure, Sci-Fi", "Jon Favreau", "126 min", "Tony Stark. Genius, billionaire, playboy, philanth...", "PG-13", "DVD", "2008", "Robert Downey Jr., Terrence Howard, Jeff Bridges, ...", 3);
        String result = instance.addMovie(title, genre, director, runtime, plot, location, rating, format, year, starring, copies, barcode, user_rating);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

//    @Test
//    public void deleteMovie() throws DAOException {
//        System.out.println("Delete Movie");
//        int id = 14;
//        MySQLMovieDAO instance = new MySQLMovieDAO();
//        Movie expResult = new Movie("elektra", "Rob Bowman");
//        String result = instance.deleteMovie(id);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
}
