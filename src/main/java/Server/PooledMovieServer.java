package Server;

import Core.ServerDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class PooledMovieServer
    {
    private ServerSocket serverSocket;
    private int listenPort;
    private int maxConnections;

    public PooledMovieServer(int listenPort, int maxConnections)
        {
            this.listenPort = listenPort;
            this.maxConnections = maxConnections;
        }

    public static void main(String[] args)
        {
        PooledMovieServer server = new PooledMovieServer(ServerDetails.SERVERPORT, ServerDetails.MAXCONNECTIONS);

        server.setUpHandlers();

        server.acceptConnections();
        }

    public void setUpHandlers()
        {
        for (int i = 0; i < ServerDetails.MAXCONNECTIONS; ++i)
            {
            PooledConnectionHandeler currentHandler = new PooledConnectionHandeler();
            Thread t = new Thread(currentHandler);
            t.start();
            }
        }
    public void acceptConnections()
        {
        try
            {
            ServerSocket server = new ServerSocket(ServerDetails.SERVERPORT, 5);
            Socket incomingConnection = null;

            while (true)
                {
                incomingConnection = server.accept();

                handleConnection(incomingConnection);
                }
            }
            catch (IOException ioe)
                {
                ioe.printStackTrace();
                }
        }
    public void handleConnection(Socket connectionToHandle)
        {

        PooledConnectionHandeler.processRequest(connectionToHandle);
        }
    }
