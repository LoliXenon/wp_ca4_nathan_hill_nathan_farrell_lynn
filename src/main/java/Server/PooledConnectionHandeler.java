package Server;

import Core.ServerDetails;
import DAO.MySQLMovieDAO;
import DAO.MySQLUserDAO;
import DTO.Movie;
import Exceptions.DAOException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class PooledConnectionHandeler implements Runnable
    {
    private Socket clientSocket;
    private MySQLUserDAO userDao = new MySQLUserDAO();

    private static List pool = new LinkedList();

    public PooledConnectionHandeler()
        {
        }

    public static void processRequest(Socket incomingClient)
        {
        synchronized (pool)
            {
            pool.add(pool.size(), incomingClient);

            pool.notifyAll();
            }
        }

    @Override
    public void run()
        {
        while (true)
            {
            synchronized (pool)
                {
                while (pool.isEmpty())
                    {
                    try
                        {
                        pool.wait();
                        }
                    catch (InterruptedException ie)
                        {
                        return;
                        }
                    }
                clientSocket = (Socket) pool.remove(0);
                }
            handleConnection();
            }
        }

    public void handleConnection()
        {
        try
            {

            String incomingCommand = "";
            String returnMessage = "";
            JSONObject returnObject = null;
            List<Movie> returnArray = null;

            boolean connected = true;

            OutputStream out = clientSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

            InputStream in = clientSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            System.out.println("New connection started");

            while (connected)
                {
                incomingCommand = input.nextLine();
                System.out.println("Request: " + incomingCommand);

                String[] msgArray = incomingCommand.split(ServerDetails.BREAKINGCHARACTER);
                int command = Integer.parseInt(msgArray[0]);
                ServerEnums enums = ServerEnums.values()[command];
                MySQLMovieDAO movies = new MySQLMovieDAO();
                {
                switch (enums)
                    {
                    case LOGIN:
                        System.out.println("Login Request: Logging in attempt");
                        returnMessage = userDao.findUserByEmailPassword(msgArray[1], msgArray[2]).toString();
                        output.println(returnMessage);
                        output.flush();
                        break;

                    case REGISTER:
                        System.out.println("Registration request: Adding new user and logging them in");
                        returnMessage = userDao.addNewUser(msgArray[1], msgArray[2], msgArray[3], msgArray[4]).toString();
                        output.println(returnMessage);
                        output.flush();
                        break;

                    case FINDMOVIEBYTITLEDIRECTOR:
                        System.out.println("Find request: Title, Director");
                        returnMessage = movies.findMovieByTitleDirector(msgArray[1], msgArray[2]);
                        output.println(returnMessage);
                        output.flush();
                        break;

                    case ADDMOVIE:
                        System.out.println("Addition Request: Movie on movies table");
                        returnMessage = movies.addMovie(msgArray[1],msgArray[2],msgArray[3],msgArray[4],msgArray[5],msgArray[6],msgArray[7],msgArray[8],msgArray[9],msgArray[10],Integer.parseInt(msgArray[11]),msgArray[12],msgArray[13]).toString();
                        output.println(returnMessage);
                        output.flush();
                        break;

                    case DELETEMOVIE:
                        System.out.println("Deletion Request: Movie on movies table");
                        returnMessage = movies.deleteMovie(Integer.parseInt(msgArray[1]));
                        output.println(returnMessage);
                        output.flush();
                        break;

                    case UPDATEMOVIE:
                        System.out.println("Update Request: Movie on movies table");
                        returnMessage = "0%%Uncoded feature";
                        output.println(returnMessage);
                        output.flush();
                        break;

                    case QUIT:
                        System.out.println("Quit request: Closing connection.");
                        connected = false;
                        break;

                    case FINDMOVIEBYDIRECTOR:
                        returnMessage = "";
                        System.out.println("Find request: Director");
                        returnArray = movies.findMovieByDirector(msgArray[1]);
                        for (int i = 0; i< returnArray.size()-1;i++)
                            {
                            returnMessage += ""+ returnArray.get(i).toString()+"%%";
                            }
                        output.println(returnMessage);
                        output.flush();
                        break;

                    case FINDMOVIEBYTITLE:
                        returnMessage = "";
                        System.out.println("Find request: Title");
                        returnArray = movies.findMovieByTitle(msgArray[1]);
                        for (int i = 0; i< returnArray.size()-1;i++)
                            {
                            returnMessage += ""+ returnArray.get(i).toString()+"%%";
                            }
                        output.println(returnMessage);
                        output.flush();
                    }

                }
                }
            output.close();
            out.close();
            input.close();
            in.close();
            clientSocket.close();
            }
        catch (IOException ioe)
            {
            ioe.printStackTrace();
            }
        catch (DAOException daoe)
            {
            daoe.printStackTrace();
            }


        }
    }
