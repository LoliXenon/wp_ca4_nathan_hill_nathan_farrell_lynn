package DTO;

public class Movie
    {
    private int movieID;
    private String title;
    private String genre;
    private String director;
    private String runtime;
    private String plot;
    private String location;
    private String ageRating;
    private String format;
    private String year;
    private String starring;
    private int copies;
    private String barcode;
    private String userRating;

    public Movie(int movieID, String title, String genre, String director, String runtime, String plot, String location,  String ageRating, String format, String year, String starring, int copies, String barcode, String userRating)
        {
        this.movieID = movieID;
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.runtime = runtime;
        this.plot = plot;
        this.location = location;
        this.ageRating = ageRating;
        this.format = format;
        this.year = year;
        this.starring = starring;
        this.copies = copies;
        this.barcode = barcode;
        this.userRating = userRating;
        }

    public Movie(String title, String genre, String director, String runtime, String plot, String location, String ageRating, String format, String year, String starring, int copies, String barcode, String userRating)
        {
        this.movieID = 0;
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.runtime = runtime;
        this.plot = plot;
        this.location = location;
        this.ageRating = ageRating;
        this.format = format;
        this.year = year;
        this.starring = starring;
        this.copies = copies;
        this.barcode = barcode;
        this.userRating = userRating;
        }

        public Movie(String iron_man, String s, String jon_favreau, String s1, String s2, String s3, String dvd, String s4, String s5, int i) {
        }


        public int getMovieID()
        {
        return movieID;
        }

    public void setMovieID(int movieID)
        {
        this.movieID = movieID;
        }

    public String getTitle()
        {
        return title;
        }

    public void setTitle(String title)
        {
        this.title = title;
        }

    public String getGenre()
        {
        return genre;
        }

    public void setGenre(String genre)
        {
        this.genre = genre;
        }

    public String getDirector()
        {
        return director;
        }

    public void setDirector(String director)
        {
        this.director = director;
        }

    public String getRuntime()
        {
        return runtime;
        }

    public void setRuntime(String runtime)
        {
        this.runtime = runtime;
        }

    public String getPlot()
        {
        return plot;
        }

    public void setPlot(String plot)
        {
        this.plot = plot;
        }

    public String getLocation()
        {
        return location;
        }

    public void setLocation(String location)
        {
        this.location = location;
        }

    public String getAgeRating()
        {
        return ageRating;
        }

    public void setAgeRating(String ageRating)
        {
        this.ageRating = ageRating;
        }

    public String getFormat()
        {
        return format;
        }

    public void setFormat(String format)
        {
        this.format = format;
        }

    public String getYear()
        {
        return year;
        }

    public void setYear(String year)
        {
        this.year = year;
        }

    public String getStarring()
        {
        return starring;
        }

    public void setStarring(String starring)
        {
        this.starring = starring;
        }

    public int getCopies()
        {
        return copies;
        }

    public void setCopies(int copies)
        {
        this.copies = copies;
        }

    public String getBarcode()
        {
        return barcode;
        }

    public void setBarcode(String barcode)
        {
        this.barcode = barcode;
        }

    public String getUserRating()
        {
        return userRating;
        }

    public void setUserRating(String userRating)
        {
        this.userRating = userRating;
        }

    @Override
    public String toString()
        {
        return "Movie{" +
                "movieID= " + movieID +
                ", title= " + title  +
                ", genre= " + genre  +
                ", director= " + director  +
                ", runtime= " + runtime  +
                ", plot=" + plot  +
                ", location= " + location  +
                ", ageRating= " + ageRating  +
                ", format= " + format +
                ", year= " + year +
                ", starring= " + starring  +
                ", copies= " + copies +
                ", barcode= " + barcode  +
                ", userRating= " + userRating +
                "}";
        }
    }
