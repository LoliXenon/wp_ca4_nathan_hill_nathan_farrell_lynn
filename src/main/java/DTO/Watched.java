package DTO;

import java.sql.Timestamp;
import java.util.Objects;


public class
Watched
{
    private Timestamp timestamp;
    private int movieID;
    private String title;
    private String genre;
    private String director;

    public Watched(Timestamp timestamp,int movieID, String title, String genre, String director) {
        this.timestamp = timestamp;
        this.movieID = movieID;
        this. title =  title;
        this.genre = genre;
        this.director = director;
    }

    public Watched(String title, String genre)
    {
        this.title = title;
        this.genre = genre;
    }

    public Watched(int movieID, String title)
    {
        this.movieID = movieID;
        this.title = title;
    }

    public Watched() {

    }

    public int GATEmovieID() {
        return movieID;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public void setmovieID(int id) {
        this.movieID = movieID;
    }

    public void setTitle(String  title) {
        this. title = title;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setDirector(String director) {
        this.director = director;
    }



    @Override
    public String toString() {
        return "movie{" + "id=" + movieID + ",  title=" +  title + ", genre=" + genre + ", director=" + director + '}';
    }

//     public String toJson() {
//        return "{"
//                + "\"id\":" + this.id +","
//                + "\"title\":" + "\"" + this.title + "\","
//                + "\"genre\":" + "\"" + this.genre + "\","
//                + "}";
//    }
//

}