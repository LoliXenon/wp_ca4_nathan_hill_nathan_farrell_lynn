package JSON;

import DTO.Movie;

import javax.json.*;
import java.util.List;

public class MovieJSON
    {
    /**
     * Build a JSON string from the data above note the escape characters -> we
     * enter \" to produce " We must build the JSON array to contain the list of
     * objects
     */
    public static String buildJsonByAppendingStrings(List<Movie> list)
        {
        String jsonString = "{" // root object
                + "\"movies\":[";   // set up the array (list)
        int i;
        for (i = 0; i < list.size() - 1; i++)
            {
            jsonString += "{"
                    + "\"id\":" + list.get(i).getMovieID() + ","
                    + "\"title\":" + "\"" + list.get(i).getTitle() + "\","
                    + "\"genre\":" + "\"" + list.get(i).getGenre() + "\","
                    + "\"director\":" + "\"" + list.get(i).getDirector() + "\""
                    + "\"runtime\":" + "\"" + list.get(i).getRuntime() + "\""
                    + "\"plot\":" + "\"" + list.get(i).getPlot() + "\""
                    + "\"location\":" + "\"" + list.get(i).getLocation() + "\""
                    + "\"ageRating\":" + "\"" + list.get(i).getAgeRating() + "\""
                    + "\"format\":" + "\"" + list.get(i).getFormat() + "\""
                    + "\"year\":" + "\"" + list.get(i).getYear() + "\""
                    + "\"starring\":" + "\"" + list.get(i).getStarring() + "\""
                    + "\"copies\":" + "\"" + list.get(i).getCopies() + "\""
                    + "\"barcode\":" + "\"" + list.get(i).getBarcode() + "\""
                    + "\"userRating\":" + "\"" + list.get(i).getUserRating() + "\""
                    + "}" + ",";

            // jsonString += list.get(i).toJson() + ",";  // could use toJson() if defined in Movie class
            }

        jsonString += "{"
                + "\"id\":" + list.get(i).getMovieID() + ","
                + "\"title\":" + "\"" + list.get(i).getTitle() + "\","
                + "\"genre\":" + "\"" + list.get(i).getGenre() + "\","
                + "\"director\":" + "\"" + list.get(i).getDirector() + "\""
                + "\"runtime\":" + "\"" + list.get(i).getRuntime() + "\""
                + "\"plot\":" + "\"" + list.get(i).getPlot() + "\""
                + "\"location\":" + "\"" + list.get(i).getLocation() + "\""
                + "\"ageRating\":" + "\"" + list.get(i).getAgeRating() + "\""
                + "\"format\":" + "\"" + list.get(i).getFormat() + "\""
                + "\"year\":" + "\"" + list.get(i).getYear() + "\""
                + "\"starring\":" + "\"" + list.get(i).getStarring() + "\""
                + "\"copies\":" + "\"" + list.get(i).getCopies() + "\""
                + "\"barcode\":" + "\"" + list.get(i).getBarcode() + "\""
                + "\"userRating\":" + "\"" + list.get(i).getUserRating() + "\""
                + "}" + ",";

        //jsonString += list.get(i).toJson();  // could use toJson() if defined in Movie class
        jsonString += "]}";  // close the array and close the object

        return jsonString;
        }

    /**
     * Converts a list of Movie objects into a JSON String
     * (approach is more difficult in this case, as make an
     * arrayBuilder, and add new objects in the loop.
     * Outside the loop, we invoke build() on the arrayBuilder
     * to create the JsonArray object. It, is then added to
     * the outer 'Root' object.
     * We 'kind of' have to build from the inside out.
     * <p>
     * Not sure if this is easier than appending strings??
     *
     * @param list list of Movie objects
     * @return
     */
    public static String buildJsonUsingJsonObjectBuilder(List<Movie> list)
        {
        JsonBuilderFactory factory = Json.createBuilderFactory(null);
        JsonArrayBuilder arrayBuilder = factory.createArrayBuilder();

        for (Movie m : list)
            {
            arrayBuilder.add(Json.createObjectBuilder()
                    .add("id", m.getMovieID())
                    .add("title", m.getTitle())
                    .add("genre", m.getGenre())
                    .add("director", m.getDirector())
                    .add("runtime", m.getRuntime())
                    .add("plot", m.getPlot())
                    .add("location", m.getLocation())
                    .add("ageRating", m.getAgeRating())
                    .add("format", m.getFormat())
                    .add("year", m.getYear())
                    .add("starring", m.getStarring())
                    .add("copies", m.getCopies())
                    .add("barcode", m.getBarcode())
                    .add("userRating", m.getUserRating())
                    .build()
            );
            }

        JsonArray jsonArray = arrayBuilder.build();

        JsonObject jsonRootObject
                = Json.createObjectBuilder()
                .add("movies", jsonArray)
                .build();

        return jsonRootObject.toString();
        }
    }
