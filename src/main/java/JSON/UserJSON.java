package JSON;

import DTO.User;

import javax.json.*;
import java.util.List;

public class UserJSON
    {
    /**
            * Build a JSON string from the data above note the escape characters -> we
     * enter \" to produce " We must build the JSON array to contain the list of
     * objects
     */
    public static String buildJsonByAppendingStrings(List<User> list)
        {
        String jsonString = "{" // root object
                + "\"users\":[";   // set up the array (list)
        int i;
        for (i = 0; i < list.size() - 1; i++) {
        jsonString += "{"
                + "\"id\":" + list.get(i).getID() + ","
                + "\"firstname\":" + "\"" + list.get(i).getFirstName() + "\","
                + "\"lastname\":" + "\"" + list.get(i).getLastName() + "\","
                + "\"email\":" + "\"" + list.get(i).getEmail() + "\""
                + "}" + ",";

        // jsonString += list.get(i).toJson() + ",";  // could use toJson() if defined in User class
        }

        jsonString += "{"
                + "\"id\":" + list.get(i).getID() + ","
                + "\"firstname\":" + "\"" + list.get(i).getFirstName() + "\","
                + "\"lastname\":" + "\"" + list.get(i).getLastName() + "\","
                + "\"email\":" + "\"" + list.get(i).getEmail() + "\""
                + "}";

        //jsonString += list.get(i).toJson();  // could use toJson() if defined in User class
        jsonString += "]}";  // close the array and close the object

        return jsonString;
        }

    /**
     * Converts a list of User objects into a JSON String
     * (approach is more difficult in this case, as make an
     * arrayBuilder, and add new objects in the loop.
     * Outside the loop, we invoke build() on the arrayBuilder
     * to create the JsonArray object. It, is then added to
     * the outer 'Root' object.
     * We 'kind of' have to build from the inside out.
     *
     * Not sure if this is easier than appending strings??
     *
     *
     * @param list  list of User objects
     * @return
     */
    public static String buildJsonUsingJsonObjectBuilder(List<User> list)
        {
        JsonBuilderFactory factory = Json.createBuilderFactory(null);
        JsonArrayBuilder arrayBuilder = factory.createArrayBuilder();

        for (User u : list) {
        arrayBuilder.add(Json.createObjectBuilder()
                .add("id", u.getID())
                .add("firstname", u.getFirstName())
                .add("lastname", u.getLastName())
                .add("email", u.getEmail())
                .build()
        );
        }

        JsonArray jsonArray = arrayBuilder.build();

        JsonObject jsonRootObject
                = Json.createObjectBuilder()
                .add("movies", jsonArray)
                .build();

        return jsonRootObject.toString();
        }
    }
