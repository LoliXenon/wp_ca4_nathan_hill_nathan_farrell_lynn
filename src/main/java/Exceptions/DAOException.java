package Exceptions;

/** A 'homemade' Exception to report exceptions
 *  arising in the the Data Access Layer.
 *
 */

import java.sql.SQLException;

public class DAOException extends SQLException
    {
    public DAOException(String aMessage)
        {
        super(aMessage);
        }
    }