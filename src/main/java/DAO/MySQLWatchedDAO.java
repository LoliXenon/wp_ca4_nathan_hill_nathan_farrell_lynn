package DAO;


import DTO.Watched;
import Exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MySQLWatchedDAO extends MySQLDAO implements WatchedDAOInterface{



    @Override
    public void watchMovie(String username, int movieId) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            //Get connection object using the methods in the super class (MySqlDao.java)...
            con = this.getConnection();

            String query = "INSERT INTO WATCHED (id, username, movieId) VALUES(null, ?,?)";

            ps = con.prepareStatement(query);
            ps.setString(1, username);
            ps.setInt(2, movieId);
            //Using a PreparedStatement to execute SQL...
            ps.executeUpdate();

        }
        catch (SQLException e)
        {
            throw new DAOException("watchMovie() " + e.getMessage());
        }
        finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            }
            catch (SQLException e)
            {
                throw new DAOException("watchMovie() " + e.getMessage());
            }
        }

    }

    @Override
    public List<Watched> watchMovie() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
