package DAO;

import DTO.User;
import Exceptions.DAOException;
import java.util.List;


    public interface UserDAOInterface
        {
        public List<User> findAllUsers() throws DAOException;
        public String findUserByEmailPassword(String email, String pword) throws DAOException ;
        public String addNewUser(String Email, String firstName, String lastName, String pword) throws DAOException;
        }

