package DAO;

import DTO.Movie;
import Exceptions.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLMovieDAO extends MySQLDAO implements MovieDAOInterface
    {
    public List<Movie> findAllMovies() throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Movie> movies = new ArrayList<>();

        try
            {
            //Get Connection object
            con = this.getConnection();

            String query = "SELECT * FROM movies";
            ps = con.prepareStatement(query);

            //Execute SQL with prepped statement
            rs = ps.executeQuery();
            while (rs.next())
                {
                int movieID = rs.getInt("id");
                //System.out.println(movieID);
                String title = rs.getString("title");
                //System.out.println(title);
                String genre = rs.getString("genre");
                //System.out.println(genre);
                String director = rs.getString("director");
                //System.out.println(director);
                String runtime = rs.getString("runtime");
                //System.out.println(runtime);
                String plot = rs.getString("plot");
                String location = rs.getString("location");
                String ageRating = rs.getString("rating");
                //System.out.println(ageRating);
                String format = rs.getString("format");
                //System.out.println(format);
                String year = rs.getString("year");
                //System.out.println(year);
                String starring = rs.getString("starring");
                //System.out.println(starring);
                int copies = rs.getInt("copies");
                //System.out.println(copies);
                String barcode = rs.getString("barcode");
                //System.out.println(barcode);
                String userRating = rs.getString("user_rating");
                //System.out.println(userRating);
                Movie m = new Movie(movieID, title, genre, director, runtime, plot, location, ageRating, format, year, starring, copies, barcode, userRating);
                movies.add(m);
                }
            return movies;
            }
        catch (SQLException e)
            {
            e.printStackTrace();
            }
        finally
            {
            try
                {
                if (rs != null)
                    {
                    rs.close();
                    }
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("findAllMovies() " + e.getMessage());

                }

            return movies;

            }

        }

    @Override
    public String findMovieByTitleDirector(String Title, String Director) throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie m = null;

        try
            {
            //Get Connection object
            con = this.getConnection();

            String query = "SELECT * FROM movies WHERE title = ? AND director = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, Title);
            ps.setString(2, Director);

            //Execute SQL with prepped statement
            rs = ps.executeQuery();
            while (rs.next())
                {
                int movieID = rs.getInt("id");
                String title = rs.getString("title");
                String genre = rs.getString("genre");
                String director = rs.getString("director");
                String runtime = rs.getString("runtime");
                String plot = rs.getString("plot");
                String location = rs.getString("location");
                String ageRating = rs.getString("rating");
                String format = rs.getString("format");
                String year = rs.getString("year");
                String starring = rs.getString("starring");
                int copies = rs.getInt("copies");
                String barcode = rs.getString("barcode");
                String userRating = rs.getString("user_rating");
                m = new Movie(movieID, title, genre, director, runtime, plot, location, ageRating, format, year, starring, copies, barcode, userRating);

                return m.toString();
                }
            }
        catch (SQLException e)
            {
            throw new DAOException("findMovieByTitleDirector() " + e.getMessage());
            }
        finally
            {
            try
                {
                if (rs != null)
                    {
                    rs.close();
                    }
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("findMovieByTitleDirector() " + e.getMessage());
                }
            }
        return "1%%Movie(s) Found";
        }

    public String addMovie(String title, String genre, String director, String runtime, String plot, String location, String ageRating, String format, String year, String starring, int copies, String barcode, String userRating) throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie m = null;

        try
            {
            //Get Connection object
            con = this.getConnection();

            String query = "INSERT INTO movies (title, genre, director, runtime, plot, location, rating, format, year, starring, copies, barcode, user_rating) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = con.prepareStatement(query);
            ps.setString(1, title);
            ps.setString(2, genre);
            ps.setString(3, director);
            ps.setString(4, runtime);
            ps.setString(5, plot);
            ps.setString(6, location);
            ps.setString(7, ageRating);
            ps.setString(8, format);
            ps.setString(9, year);
            ps.setString(10, starring);
            ps.setInt(11, copies);
            ps.setString(12, barcode);
            ps.setString(13, userRating);

            //Execute SQL with prepped statement
            ps.executeUpdate();
            }
        catch (SQLException e)
            {
            System.out.println("Not inserted: ");
            e.printStackTrace();
            }
        finally
            {
            try
                {
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("addMovie() " + e.getMessage());
                }


            }
        return "1%%Added item successfully";
        }


    public String deleteMovie(int id) throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Movie m = null;

        try
            {
            //Get Connection object
            con = this.getConnection();

            String query = "DELETE FROM movies WHERE id = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1, id);

            //Execute SQL with prepped statement
            ps.executeUpdate();
            }
        catch (SQLException e)
            {
            System.out.println("Not deleted: ");
            e.printStackTrace();
            }
        finally
            {
            try
                {
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("addMovie() " + e.getMessage());
                }


            }
        return "1%%Deleted item successfully";
        }

    public List<Movie> findMovieByDirector(String Director) throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Movie> movies = new ArrayList<>();

        try
            {
            //Get Connection object
            con = this.getConnection();

            String query = "SELECT * FROM movies WHERE director = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, Director);

            //Execute SQL with prepped statement
            rs = ps.executeQuery();
            while (rs.next())
                {
                int movieID = rs.getInt("id");
                String title = rs.getString("title");
                String genre = rs.getString("genre");
                String director = rs.getString("director");
                String runtime = rs.getString("runtime");
                String plot = rs.getString("plot");
                String location = rs.getString("location");
                String ageRating = rs.getString("rating");
                String format = rs.getString("format");
                String year = rs.getString("year");
                String starring = rs.getString("starring");
                int copies = rs.getInt("copies");
                String barcode = rs.getString("barcode");
                String userRating = rs.getString("user_rating");
                Movie m = new Movie(movieID, title, genre, director, runtime, plot, location, ageRating, format, year, starring, copies, barcode, userRating);
                movies.add(m);
                }
            return movies;
            }
        catch (SQLException e)
            {
            throw new DAOException("findMovieByDirector() " + e.getMessage());
            }
        finally
            {
            try
                {
                if (rs != null)
                    {
                    rs.close();
                    }
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("findMovieByDirector() " + e.getMessage());
                }
            }
        }

    public List<Movie> findMovieByTitle(String Title) throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Movie> movies = new ArrayList<>();

        try
            {
            //Get Connection object
            con = this.getConnection();

            String query = "SELECT * FROM movies WHERE title = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, Title);

            //Execute SQL with prepped statement
            rs = ps.executeQuery();
            while (rs.next())
                {
                int movieID = rs.getInt("id");
                String title = rs.getString("title");
                String genre = rs.getString("genre");
                String director = rs.getString("director");
                String runtime = rs.getString("runtime");
                String plot = rs.getString("plot");
                String location = rs.getString("location");
                String ageRating = rs.getString("rating");
                String format = rs.getString("format");
                String year = rs.getString("year");
                String starring = rs.getString("starring");
                int copies = rs.getInt("copies");
                String barcode = rs.getString("barcode");
                String userRating = rs.getString("user_rating");
                Movie m = new Movie(movieID, title, genre, director, runtime, plot, location, ageRating, format, year, starring, copies, barcode, userRating);
                movies.add(m);
                }
            return movies;
            }
        catch (SQLException e)
            {
            throw new DAOException("findMovieByDirector() " + e.getMessage());
            }
        finally
            {
            try
                {
                if (rs != null)
                    {
                    rs.close();
                    }
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("findMovieByTitle() " + e.getMessage());
                }
            }
        }

    }



