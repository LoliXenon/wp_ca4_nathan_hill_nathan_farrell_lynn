package DAO;

import DTO.Movie;
import Exceptions.DAOException;

import java.util.List;

public interface MovieDAOInterface
    {
    public List<Movie> findAllMovies() throws DAOException;
    public String findMovieByTitleDirector(String title, String director) throws DAOException ;
    public String addMovie(String title, String genre, String director, String runtime, String plot, String location, String ageRating, String format, String year, String starring, int copies, String barcode, String userRating) throws DAOException;
    public String deleteMovie(int id) throws DAOException;
    public List<Movie> findMovieByDirector(String Director) throws DAOException;
    public List<Movie> findMovieByTitle(String Title) throws DAOException;
    }
