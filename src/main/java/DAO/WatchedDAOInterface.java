package DAO;

import DTO.Watched;
import Exceptions.DAOException;
import java.util.List;

public interface WatchedDAOInterface {
    public List<Watched> watchMovie() throws DAOException;
    public void watchMovie(String username, int movieId) throws DAOException;


}
