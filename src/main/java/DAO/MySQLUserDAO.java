package DAO;

import DTO.User;
import Exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MySQLUserDAO extends MySQLDAO implements UserDAOInterface
    {
    public List<User> findAllUsers() throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<>();

        try
            {
            //Get Connection object
            con = this.getConnection();

            String query = "SELECT * FROM users";
            ps = con.prepareStatement(query);

            //Execute SQL with prepped statement
            rs = ps.executeQuery();
            while (rs.next())
                {
                int userId = rs.getInt("user_ID");
                String email = rs.getString("email");
                String password = rs.getString("password");
                String lastname = rs.getString("last_name");
                String firstname = rs.getString("first_name");
                User u = new User(userId, firstname, lastname, email, password);
                users.add(u);
                }
            }
        catch (SQLException e)
            {
            throw new DAOException("findAllUsers() " + e.getMessage());
            }
        finally
            {
            try
                {
                if (rs != null)
                    {
                    rs.close();
                    }
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("findAllUsers() " + e.getMessage());
                }
            }
        return users;     // may be empty
        }

    @Override
    public String findUserByEmailPassword(String Email, String pword) throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;
        try {
        con = this.getConnection();

        String query = "SELECT * FROM users WHERE email = ? AND password = ?";
        ps = con.prepareStatement(query);
        ps.setString(1, Email);
        ps.setString(2, pword);

        rs = ps.executeQuery();
        if (rs.next())
            {
            int userId = rs.getInt("user_ID");

            return "" + userId+"%%Logged In";
//            int userId = rs.getInt("user_ID");
//            String email = rs.getString("email");
//            String password = rs.getString("password");
//            String lastname = rs.getString("last_name");
//            String firstname = rs.getString("first_name");
//            u = new User(userId, firstname, lastname, email, password);
            }
        }
        catch (SQLException e)
            {
            throw new DAOException("findUserByEmailPassword() " + e.getMessage());
            }
        finally
            {
            try
                {
                if (rs != null)
                    {
                    rs.close();
                    }
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("findUserByEmailPassword() " + e.getMessage());
                }
            }
        //return u;     // u may be null
        return "" + -1 + "%%Invalid Login";
        }

    public String addNewUser(String Email, String pword, String firstName, String lastName) throws DAOException
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;
        try {
        con = this.getConnection();

        String query = "INSERT INTO users (email, password, first_name, last_name) VALUES (?,?,?,?)";
        ps = con.prepareStatement(query);
        ps.setString(1, Email);
        ps.setString(2, pword);
        ps.setString(3,firstName);
        ps.setString(4,lastName);

        ps.executeUpdate();

        return findUserByEmailPassword(Email, pword);

        }
        catch (SQLException e)
            {
            throw new DAOException("findUserByEmailPassword() " + e.getMessage());
            }
        finally
            {
            try
                {
                if (ps != null)
                    {
                    ps.close();
                    }
                if (con != null)
                    {
                    freeConnection(con);
                    }
                }
            catch (SQLException e)
                {
                throw new DAOException("findUserByEmailPassword() " + e.getMessage());
                }
            }
        }
    }