//package BusinessObjects;
//
//import DAO.MovieDAOInterface;
//import DAO.MySQLMovieDAO;
//import DAO.MySQLUserDAO;
//import DAO.UserDAOInterface;
//import DTO.Movie;
//import DTO.User;
//import Exceptions.DAOException;
//
//import java.util.List;
//
//public class Main
//    {
//
//    public static void main(String [] args)
//        {
//        UserDAOInterface IUserDao = new MySQLUserDAO();
//        MovieDAOInterface IMovieDAO = new MySQLMovieDAO();
//
//
//            //          // Display the menu
////        System.out.println("1\t Movie By Name");
////        System.out.println("2\t Add Movie");
////        System.out.println("3\t Delete Movie");
////        System.out.println("4\t Update Movie");
////
////        System.out.println("Please enter your choice:");
////
////        int choice = 0;
////
////        //Display the title of the chosen module
////        switch (choice) {
////            case 1: System.out.println("Movie By Name");
////	   break;
////            case 2: System.out.println("Add Movie");
////                    break;
////            case 3: System.out.println("Delete Movie");
////                    break;
////            case 4: System.out.println("Update Movie");
////                     break;
////            default: System.out.println("Invalid choice");
////        }//end of switch
//
//        try
//            {
//            List<User> users = IUserDao.findAllUsers();
//
//            if( users.isEmpty() )
//                System.out.println("There are no Users");
//
//            for( User user : users )
//                System.out.println("User: " + user.toString());
//
//            // test DAO with email and password
//            User user = IUserDao.findUserByEmailPassword("eatpant@pant.eat", "eatpant1");
//            if(user != null)
//                System.out.println("User found: " + user);
//            else
//                System.out.println("Username with that password not found");
//
//            // test DAO with bad email
//            user = IUserDao.findUserByEmailPassword("madmax", "thunderdome");
//            if(user != null)
//                System.out.println("User found: " + user);
//            else
//                System.out.println("Username with that password not found");
//
//
//
//            }
//        catch( DAOException e )
//            {
//            e.printStackTrace();
//            }
//        try
//            {
//            List<Movie> movies = IMovieDAO.findAllMovies();
//
//            if (movies.isEmpty())
//                {
//                System.out.println("There are no movies");
//                }
//
//            for (Movie movie : movies)
//                {
//                System.out.println("Movie: " + movie.toString());
//                }
//
//            //test DAO with title and director
//            Movie movie = IMovieDAO.findMovieByTitleDirector("iron man", "Jon Favreau");
//            if (movie != null)
//                System.out.println("Movie found: " + movie);
//            else
//                System.out.println("Movie directed by that director not found");
//            }
//            catch (DAOException e)
//                {
//                e.printStackTrace();
//
//                }
//        }
//    }
//
