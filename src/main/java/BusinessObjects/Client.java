package BusinessObjects;

import DTO.Movie;
import Server.PooledConnectionHandeler;

import java.io.*;
import java.net.Socket;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Client
    {
    int id = -1; // entered by user once, store for life of client
    boolean running = true;

    public static void main(String[] args)
        {
        Client client = new Client();
        client.start();
        }

    public void start()
        {
        Scanner in = new Scanner(System.in);
        try
            {
            Socket socket = new Socket("localhost", 50000);  // connect to server socket, and open new socket
            OutputStream os = socket.getOutputStream();
            PrintWriter socketWriter = new PrintWriter(os, true);// true=> auto flush buffers

            InputStream is = socket.getInputStream();
            BufferedReader socketReader = new BufferedReader((new InputStreamReader(is)));




            System.out.println("Client: Port# of this client : " + socket.getLocalPort());
            System.out.println("Client: Port# of Server :" + socket.getPort());

            System.out.println("\nClient: This Client is running and has connected to the server");
            System.out.println("\n \n Welcome: Please enter an option");
            System.out.println("1\t Login");
            System.out.println("2\t Register");
            System.out.println("3\t Quit");

            while (running)
                {
                String init = in.next();
                if (init.matches("\\d"))
                    {
                    int initial = Integer.parseInt(init);
                    switch (initial)
                        {
                        case 1:
                            login(socketReader, socketWriter);
                            break;
                        case 2:
                            register(socketReader, socketWriter);
                            break;
                        case 3:
                            System.out.println("Closing down");
                            running = false;
                            break;
                        default:
                            System.out.println("Invalid input");

                        }
                    } else
                    {
                    System.out.println("Invalid input");
                    }

                }
            }
        catch(IOException ioe)
            {
            ioe.printStackTrace();

            }

            }


    public void systemMenu(Integer id, PrintWriter writer, BufferedReader buffered)
        {
        Scanner intCmd = new Scanner(System.in);
        Scanner strCmd = new Scanner(System.in);
        String[] response;
        try
            {
            while (id >= 0)
                {
                //Display the menu
                System.out.println("1\t Find menu");
                System.out.println("2\t Add a Movie");
                System.out.println("3\t Delete Movie");
                System.out.println("4\t Update a Movie");
                System.out.println("5\t Add to watched list");
                System.out.println("6\t Close and Log out");

                System.out.println("Please enter your choice:");
                String command = intCmd.next();

                if (command.matches("\\d"))
                    {
                    int cmd = Integer.parseInt(command);

                    //Display the title of the chosen module
                    switch (cmd)
                        {
                        case 1:
                            findMenu(strCmd, intCmd, writer, buffered);
                            break;

                        case 2:
                            addMovie(strCmd, intCmd, writer, buffered);
                            break;

                        case 3:
                            deleteMovie(strCmd, intCmd, writer, buffered);
                            break;

                        case 4:
                            System.out.println("Update Movie > Uncoded");
                            writer.println("5%%");
                            response = buffered.readLine().split("%%");
                            System.out.println(response[1]);
                            break;
                        case 5:
                            addWatched(strCmd, intCmd, writer, buffered);
                            break;
                        case 6:
                            System.out.println("Logging out");
                            writer.println("6%%");
                            id = -1;
                            running = false;
                            break;
                        default:
                            System.out.println("Invalid choice");
                        }
                    //end of switch
                    }
                else
                    {
                    System.out.println("Invalid input");
                    }
                    }

            }
        catch (IOException ioe)
            {
            ioe.printStackTrace();
            }
        }

    public void findMenu(Scanner strCmd, Scanner intCmd, PrintWriter writer, BufferedReader buffered)
        {
        try
            {
            System.out.println("Find By:");
            System.out.println("1\t Title and Director");
            System.out.println("2\t Director");
            System.out.println("3\t Title");
            System.out.println("4\t Return");
            String findCommand = intCmd.next();

            if (findCommand.matches("\\d"))
                {
                int command = Integer.parseInt(findCommand);
                switch (command)
                    {
                    case 1:
                        findByTitleDirector(strCmd, intCmd, writer, buffered);
                        break;
                    case 2:
                        findByDirector(strCmd, intCmd, writer, buffered);
                        break;
                    case 3:
                        findByTitle(strCmd, intCmd, writer, buffered);
                        break;
                    case 4:
                        break;
                    default:
                        System.out.println("Invalid input");
                    }
                }
            else
                {
                System.out.println("Invalid input");
                }


            }
            catch (Exception e)
                {
                e.printStackTrace();
                }
        }


    public void login(BufferedReader socketReader, PrintWriter socketWriter)
        {
        Scanner login = new Scanner(System.in);
        try
            {

            String email;
            String pass;
            System.out.println("Please enter email: ");
            email = login.nextLine();  // read a username from the user

            System.out.println("Please enter password: ");
            pass = login.nextLine();   // read an email from the user

            socketWriter.println("0%%" + email + "%%" + pass);
            String[] response = socketReader.readLine().split("%%");


            if (Integer.parseInt(response[0]) >= 0)
                {
                id = Integer.parseInt(response[0]);
                System.out.println(response[1]);
                systemMenu(id, socketWriter, socketReader);
                } else
                {
                System.out.println(response[1]);
                }

            }
        catch (IOException e)

            {
            System.out.println("Client message: IOException: " + e);
            }
        }

    public void register(BufferedReader socketReader, PrintWriter socketWriter)
        {
        Scanner register = new Scanner(System.in);

        try
            {
            String email;
            String pass;
            String firstName;
            String lastName;

            System.out.println("Please enter email: ");
            email = register.nextLine();

            System.out.println("Please enter password: ");
            pass = register.nextLine();

            System.out.println("Please enter your first name");
            firstName = register.nextLine();

            System.out.println("Please enter your last name");
            lastName = register.nextLine();

            socketWriter.println("1%%" + email + "%%" + pass + "%%" + firstName + "%%" + lastName);
            String[] response = socketReader.readLine().split("%%");

            if (Integer.parseInt(response[0]) >= 0)
                {
                id = Integer.parseInt(response[0]);
                System.out.println(response[1]);
                systemMenu(id, socketWriter, socketReader);
                } else
                {
                System.out.println(response[1]);
                }
            }
            catch (IOException ioe)
                {
                ioe.printStackTrace();
                }
        }

    public void addMovie(Scanner strCmd, Scanner intCmd, PrintWriter writer, BufferedReader buffered)
        {
        try
            {

            String[] response;

            System.out.println("Add Movie");
            System.out.println("Enter title");
            String title = strCmd.nextLine();
            System.out.println("Enter genres");
            String genre = strCmd.nextLine();
            System.out.println("Enter director");
            String director = strCmd.nextLine();
            System.out.println("Enter runtime");
            String runtime = strCmd.nextLine();
            System.out.println("Enter plot");
            String plot = strCmd.nextLine();
            System.out.println("Enter location");
            String location = strCmd.nextLine();
            System.out.println("Enter age rating");
            String ageRating = strCmd.nextLine();
            System.out.println("Enter format");
            String format = strCmd.nextLine();
            System.out.println("Enter year of release");
            String year = strCmd.nextLine();
            System.out.println("Enter actors starring");
            String starring = strCmd.nextLine();
            System.out.println("Enter number of copies owned");
            String copy = intCmd.next();
            int copies = 0;
            if (copy.matches("\\d"))
                {
                copies = Integer.parseInt(copy);
                }
            else
                {
                System.out.println("invalid input");
                }
            System.out.println("Enter barcode");
            String barcode = strCmd.nextLine();
            System.out.println("Enter user rating");
            String userRating = strCmd.nextLine();

            String addMovie = "3%%" + title + "%%" + genre + "%%" + director + "%%" + runtime + "%%" + plot + "%%" + location + "%%" + ageRating + "%%"
                    + format + "%%" + year + "%%" + starring + "%%" + copies + "%%" + barcode + "%%" + userRating;
            writer.println(addMovie);
            response = buffered.readLine().split("%%");
            System.out.println(response[1]);
            }
            catch (IOException ioe)
                {
                ioe.printStackTrace();
                }
        }

    public void deleteMovie(Scanner strCmd, Scanner intCmd, PrintWriter writer, BufferedReader buffered)
        {
        try
            {
            String[] response;

            System.out.println("Delete Movie");
            System.out.println("Enter ID of movie to be deleted");
            int deleteID = intCmd.nextInt();
            writer.println("4%%" + deleteID);
            response = buffered.readLine().split("%%");
            System.out.println(response[1]);
            }
            catch (IOException ioe)
                {
                ioe.printStackTrace();
                }
        }

    public void findByTitleDirector(Scanner strCmd, Scanner intCmd, PrintWriter writer, BufferedReader buffered)
        {
        try
            {

            String[] response;

            System.out.println("Movie by Title and Director");
            System.out.println("Enter the title of the movie");
            String entry1 = strCmd.nextLine();
            System.out.println("Enter the director of the movie");
            String entry2 = strCmd.nextLine();
            String dispatch = "2%%" + entry1 + "%%" + entry2;
            writer.println(dispatch);
            response = buffered.readLine().split("%%");
            for (int i = 0; i < response.length; i++)
                {
                System.out.print(response[i] + "\n");
                }
            }
            catch (IOException ioe)
                {
                ioe.printStackTrace();
                }
        }


    public void findByDirector(Scanner strCmd, Scanner intCmd, PrintWriter writer, BufferedReader buffered)
        {
        try
            {

            String[] response;

            System.out.println("Movies by Director");
            System.out.println("Enter the director of the movie");
            String entry = strCmd.nextLine();
            String dispatch = "7%%" + entry;
            writer.println(dispatch);

            response = buffered.readLine().split("%%");
            for (String movie : response)
                {
                System.out.print(movie + "\n");
                }
            System.out.println("\n");
            }
        catch (IOException ioe)
            {
            ioe.printStackTrace();
            }
        }

    public void findByTitle(Scanner strCmd, Scanner intCmd, PrintWriter writer, BufferedReader buffered)
        {
        try
            {

            String[] response;


            System.out.println("Movies by Title");
            System.out.println("Enter the title of the movie");
            String entry = strCmd.nextLine();
            String dispatch = "8%%" + entry;
            writer.println(dispatch);

            response = buffered.readLine().split("%%");
            for (String movie : response)
                {
                System.out.print(movie + "\n");
                }
            System.out.println("\n");
            }
        catch (IOException ioe)
            {
            ioe.printStackTrace();
            }
        }

    public void addWatched(Scanner strCmd, Scanner intCmd, PrintWriter writer, BufferedReader buffered)
        {

        String[] response;

        try
            {
            System.out.println("Add Watched");
            System.out.println("Enter Movie ID");
            int movieId = intCmd.nextInt();
            System.out.println("ID : " + movieId);
            writer.println("9%%" + movieId);
            response = buffered.readLine().split("%%");
            System.out.println(response[1]);
            }
            catch (IOException ioe)
                {
                ioe.printStackTrace();
                }

        }

    }